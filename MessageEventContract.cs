﻿using System;
using JubloAps.MessageEvent.Contracts.Types;
using JubloAps.Shared.Aspects;
using PostSharp.Patterns.Contracts;

namespace JubloAps.MessageEvent.Contracts
{
    public class MessageEventContract
    {
        [NotNull]
        public MessageEventType MessageEventType { get; set; }
        [NotEmpty]
        public string Message { get; set; }
        [GuidNotNull]
        public Guid CaseId { get; set; }
        [GuidNotNull]
        public Guid SenderId { get; set; } //is a stakeholderId
        public DateTime Created { get; set; } = DateTime.UtcNow;
        [NotEmpty]
        public string ApiKey { get; set; }
    }
}
