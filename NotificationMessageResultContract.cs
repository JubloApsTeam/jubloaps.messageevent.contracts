﻿using System;
using System.Collections.Generic;
using System.Text;
using JubloAps.MessageEvent.Contracts.Types;

namespace JubloAps.MessageEvent.Contracts
{
    public class NotificationMessageResultContract
    {
        public NotificationMessageResultType NotificationType { get; set; }
        public string Notification { get; set; }
    }
}
