﻿using System;
using System.Collections.Generic;
using System.Text;
using PostSharp.Patterns.Contracts;

namespace JubloAps.MessageEvent.Contracts
{
    public class MessageResultContract : MessageEventContract
    {
        [NotEmpty]
        public string SenderName  { get; set; }
        public List<NotificationMessageResultContract> Notifications { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime IsDeleted { get; set; }
    }
}
